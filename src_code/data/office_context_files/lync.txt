	

232	

Chapter 11	

Talking It Over with Microsoft Lync

Getting Started with Lync
The Lync 2010 window pops up on your desktop, taking up only a portion of your screen.
This design makes it easy for you to have other windows open on-screen while you’re
communicating with your colleagues in Lync. (See Figure 11-2.) You can do a number of
things right away in Lync (and you’ll learn more about these tasks in the following sections):

	 Add a personal note to tell others what you’re doing.

■

	 Change your profile picture.

■

	 Search for contacts you want to add to Lync.

■

	 Launch a phone call, instant messaging (IM) session, video chat, or meeting.

■

	 Set your online status and presence indicator.

■

	 Enter your location.

■

Set your online status.
Click to add a personal note.
Lync uses your profile picture.

Choose Lync options.
Enter your location.

Search for contacts.

FIGURE 11-2  The Lync window offers various tools for interacting with your team in real time.

		
Introducing Microsoft Lync

Entering a Personal Note
You can click in the What’s Happening Today? area at the top of the Lync window and
type a phrase about what you’re working on, how your day is going, or just about
anything else that strikes your fancy. The personal note in Lync is like a status update in
Facebook; it lets your teammates keep up with what’s going on in your world.
Simply click in the box and type the phrase you want to share; then press Enter. When
your colleagues view your contact info in Lync on their own computers, they will see your
note beside your photo. They can also get an update on your added notes by clicking the
Activity Feed tool at the top of the contacts list in the Lync window. You can update your
personal note as many times as you like throughout the day.
	

Tip	

You can also use your personal note to send quick information to your
team. For example, posting “Running late—let’s meet at 2:15!” lets others
know you need a few more minutes before the staff meeting. Or “Don’t
forget to send in your expense reports!” reminds others that they need to
submit their reports before your meeting begins.

Setting Your Location
Setting your location might not seem like that big a deal if your entire team works in
the same state, but when you’re sharing files and tasks with people all over the world,
k
­ nowing the time difference of each person’s location helps you coordinate schedules
and arrange meetings when it’s convenient for everyone.
Click Set Your Location and a text box appears so that you can type your location. You
can then click the Location arrow to see additional options, which enable you to turn off
the display of your location and remove custom locations. (See Figure 11-3.)

Chapter 11	

233

	 234	

Chapter 11	

Talking It Over with Microsoft Lync

FIGURE 11-3  Click and type your location in the Location field; then click the arrow to change Location

options.

	

Note	

If you’re an administrator, you can set Location information by editing
the settings available for each information user. Click Admin and then
choose Users. Click the name of the user whose account you want to
change, and then click Settings. Click the arrow in the Set User Location
setting, and click the location you want to assign to the user.

Setting Up Sound and Voice
Lync includes a friendly setup wizard that walks you through the process of entering your
information and setting up your version of Lync so that you can speak, hear, and see
others in your online meetings and conversations. To launch the wizard, click the Options
button in the upper right corner of the Lync window, click File, click Help, and follow the
prompts on the screen until you get to the Microsoft Lync 2010 welcome page to launch
the wizard. (See Figure 11-4.)

		
Introducing Microsoft Lync

FIGURE 11-4  Clicking Start on this page launches the Welcome To Lync Wizard, which walks you through

the process of setting up your computer for online meetings.

Click Start in the lower right corner to begin the wizard. The utility explains the personal
note, picture, and location area, and it gives you the chance to enter your mobile phone
number so that you can get Lync calls on your phone. Click in the mobile phone box, and
type your number; then click outside the box to close it.
If you haven’t already done so, plug in your headset or connect your speakers and
m
­ icrophone. Then click Get Connected, and Lync searches for the audio capability on
your system. When Lync finds your audio devices, you will see the message, “You can use
your computer’s mic and speakers for calls.”
If you have a webcam connected to your computer, you can make sure it is working
properly with Lync by clicking Webcam Check. The wizard checks your webcam to make
sure it’s working properly; you’ll be able to see the live video in the display your screen.
Click Next, and then click Exit to close the wizard and return to the Lync window.

CHOOSING ADVANCED AUDIO SETTINGS
There’s another way you can fine-tune your audio settings for Lync. Click the
Options button in the upper right corner of the Lync window, and click Tools and
then Options. In the Lync Options dialog box, click Audio Device.
On this screen, you can choose the device you want to use for calls, and choose the
devices and sound levels you prefer for your speakers, microphone, and ringer.

Chapter 11	

235

	

236	

Chapter 11	

Talking It Over with Microsoft Lync

Click your choices, and then click OK to save your settings.
	

Tip	

You can use Lync without audio or video capability, but being able to
have real-time conversations—and see others via webcam—can enhance
your communications and help your team work productively together.

Working with Contacts in Lync
After you finish entering your information and checking audio and video connections,
your next step is to add contacts to Lync. You might have already added user accounts
to Office 365, so adding those contacts to Lync is as simple as searching for the contact
names you want to add and pinning them to your contacts list. Here are the steps:
	 1.	 Sign in to Lync if you’re not logged in automatically.
	 2.	 In the Microsoft Lync window, click in the Search box.
	 3.	 Type the email address of the contact you want to add, and press Enter.
	 4.	 Click the Add To Contacts button. (See Figure 11-5.)
	 5.	 Click Pin To Frequent Contacts or All Contacts to choose where the contact will

be added.

		
Working with Contacts in Lync

FIGURE 11-5  When you type the email address of the contact, Lync searches for the contact name

and adds it to the contact list.

SO HOW DO EXTERNAL COMMUNICATIONS WORK?
By default, Lync is set up to allow external communications, which means that
you and your teammates will be able to use Lync to communicate with people
outside your Office 365 team.
For using public instant messaging with people outside of your Office 365
a
­ pproved contacts, however, Windows Live Messenger is the software that is
used. You can also set up audio and video calls with external contacts, but again,
Windows Live Messenger supports the calls. Audio and video conferences and
desktop sharing are not available when you’re working with external contacts.

Creating a Contact Group
You can create a group of contacts if you want to organize your team for a specific
p
­ roject or task; or you might group all users who are in a certain location or working with a particular program. After you create a group, you can communicate with all
m
­ embers of that group at once, for example, by sending a group email to the whole

Chapter 11	

237

	 238	

Chapter 11	

Talking It Over with Microsoft Lync

team about an upcoming meeting or event. You can create a new contact group when
you add a contact to Lync, or you can create a group and then add contacts to it as you
go along. To create a group with your existing users, follow these steps:
	 1.	 Click in the search box, and enter the email address of the user you want to add.
	 2.	 Click the Add To Contacts button, and click Add To New Group.

Lync creates a new group and selects the group name. The new user is displayed in
the group. (See Figure 11-6.)
	 3.	 Type the name you want to use for the new group, and press Enter.

FIGURE 11-6  Click Add To New Group to create a new contact group in the Lync window.

To create a new group that you will add contacts to later, right-click Frequent Contacts
and select Create New Group. Enter a name for the new group, and press Enter. You can
then drag and drop contacts into the new group when you’re ready to add members to
it.
You can include contacts in more than one group if you like, and you can also rearrange
the groups in the Lync window by simply dragging and dropping a selected group to a
new position in the list.

		
Working with Contacts in Lync

Accepting a Contact Request
When you add a new contact to your list, a contact request is sent to the person you’re
adding. Similarly, when others add you to their contact list, you receive a message asking your permission to be added to their list. The Lync message box alerting you to this
a
­ ppears on your desktop if you are online; if you’re not using Lync when someone adds
you to her list, the message will pop up the next time you log in. (See Figure 11-7.)

FIGURE 11-7  When another person adds you to her Contacts list, you receive a notification you can use
to add that person to your list as well.

If you want to add the person to your own Contacts list, click the Add To This Contact
Group check box and click the All Contacts arrow on the right side of the message box.
Choose the group to which you want to assign the new contact. Additionally, you can
choose the privacy relationship you want to apply to this contact by clicking the arrow to
the right of Colleagues and choosing the level you want to set for that person. (Read on
for more about setting privacy relationships in Lync.)

Chapter 11	

239

	 240	

Chapter 11	

Talking It Over with Microsoft Lync

Setting Privacy Levels for Contact Relationships
No matter how many contacts you add to Lync, you can tailor each contact so that it
a
­ llows others to see just the amount of information you want them to see. Lync allows
you to choose from among five different privacy settings:

	 Friends And Family  This setting gives others the most information about you,

■

enabling them to see all your contact information and viewing any items you
i
­nclude in your profile (such as picture, email address, and more). Friends and
f
­ amily cannot see your meeting details, however.

	 Workgroup  This setting is designed for the team you work with on a regular

■

basis. Contacts you assign this privacy setting to can see all your contact information but won’t be able to view your Home and Other phone numbers. This setting
also enables others to contact you even if you have your Lync status set to Do Not
Disturb.

	 Colleagues  Contacts assigned these privacy settings can see all your contact

■

information but cannot view your Home, Other, and Mobile phone numbers.
A
­ dditionally, contacts designated as Colleagues will not be able to view your
m
­ eeting details.

	 External Contacts  This setting shows others only your name, title, email address,

■

company, and picture.

	 Blocks Contacts  Contacts with this setting can see only your presence in Lync—

■

your name and email address—but they will be unable to contact you through
Lync. To communicate with you, people you designate as Blocked Contacts will
need to send you an email message.
By default, Lync sets up all new contacts with the Colleagues setting. As you saw in
the previous section, when someone adds you to his or her Contacts list, you receive a
n
­ otification, and you can set the privacy level for your relationship when you add that
person to your own Contacts list.
You can also change the privacy setting for any of your contacts. In the Lync window,
right-click the contact you want to change and point to Change Privacy Relationship.
A list of available privacy settings appears, and the current level is highlighted. (See
Figure 11-8.) Simply click the new level you want to apply or, if you want to return the
contact to the Lync default settings, click Auto-Assign Relationship.

		
Working with Contacts in Lync

FIGURE 11-8  You can change the amount of information your contacts can see about you by choosing a
different privacy setting.

Tagging Contacts
When you’re working closely with others—especially if you’re working at a distance from
them—it can sometimes be helpful to know when people are available online and when
they’re not. Suppose you’re working on finalizing a contract, but you really need the
i
­nput of a manager who is working overseas. You can tag his contact to let Lync know
you want to be alerted when he logs into Lync the next time so that you can ask him
questions as you finalize your contract. Here’s how to do that:
	 1.	 In the Lync window, right-click the contact you want to change.
	 2.	 Click Tag For Status Change Alerts. (See Figure 11-9.)

Chapter 11	

241

	 242	

Chapter 11	

Talking It Over with Microsoft Lync

FIGURE 11-9  You can tag individual contacts so that you are notified when their online status

changes.

The next time the contact signs in to Lync, an alert will appear on your desktop, letting
you know the contact is now available online. (See Figure 11-10.) Similarly, when the
contact’s status changes—to Away, Busy, or any of the other status settings—an alert will
let you know.

FIGURE 11-10  After you tag a contact so that you receive alerts, Lync lets you know when the contact’s

status changes.

		
Working with Contacts in Lync

	

Tip	

If the number of alerts you receive about status changes gets annoying,
you can simply cancel the alerts by clicking Untag. If you simply want to
close the alert box without contacting the person (it will disappear on its
own after a few seconds), you can click Ignore to close the message box.

Changing Contact Views
By default, your contacts in the Lync window appear organized by group, but as you can
see from the selections at the top of the list, you can change the view to display your
contacts arranged by online status or by relationship. The Display Options on the right
side of the views row gives you options for changing the way contact information is
organized in the current view. (See Figure 11-11.)
Contacts are organized by the
groups they are assigned to.
Contacts are displayed according
to their online status.

Contacts are grouped by the
privacy levels you have assigned to them.
Click to choose
display options

FIGURE 11-11  You can display your contacts in different ways by clicking the view tool you want to use

and choosing View Options.

Chapter 11	

243

	 244	

Chapter 11	

Talking It Over with Microsoft Lync

To change the way your contacts appear in the list, click the view you want to use
(Groups, Status, or Relationship.) The list is immediately rearranged according to your
selection. You can further tailor the information shown for your contacts by clicking the
Display Options tool and clicking the items in the list you want to display. To remove any
of the existing items, simply click it. The highlight is removed from the item, and that
piece of information is hidden from view.

Working with Presence and Contact Cards
Your presence will be visible to all others on your team—and the presence of others
will be visible to you. Others will also be able to see your presence indicator in Outlook
Web App and when you’re working collaboratively on documents in Microsoft Word,
P
­ owerPoint, and Excel. You can change your presence at any point in Office 365 where
you can see the presence indicator.
So what do the colors mean? Here’s a quick list:
A green presence indicator means that the contact is online and available,
and all contact methods through Lync are available to you.
A yellow presence indicator means that the person is away from his desk
or unavailable at the moment. Limited communications methods will be
available through Lync.
A red presence indicator means that the person is online but is currently
unavailable, either because she is busy or has set Do Not Disturb as the
s
­ elected status. Limited communication options will be available to you
when you see this presence indicator.
The presence indicator appears to the left of the profile picture in your contacts list,
which enables you to see at a glance the online status of your contact. You can click
the contact to display the person’s contact card. (See Figure 11-12.) This card shows the
v
­ arious communications methods you can use to be in touch with the person.

	

Note	

The methods of contacting someone that are available to you depend on
the person’s status. If a person’s status is Offline, you cannot send her an
instant message, but you can send an email message if you like.

		
Instant Messaging with Your Team

Click to choose communication methods.

Expand Contact Card tool

Additional contact information

FIGURE 11-12  Click the contact to display the contact card and choose a way to communicate.

Instant Messaging with Your Team
There’s a great kind of freedom inherent in instant messaging. You have a question—ask
it! Are you wondering how things are going on the new design? Send your team an IM
and find out.
Of course, for those of us who love to be in touch, instant messaging can be a bit
a
­ ddictive—and annoying to those who don’t want to be interrupted 20 times a day.
(Hence, the need for the Away or Busy statuses in Lync.)
When you want to instant message one or many people on your team, you begin in the
Lync window and follow these steps:
	 1.	 Click the contact you want to send an instant message to. The person’s contact

card appears.
	 2.	 Click the instant message icon.

Chapter 11	

245

	 246	

Chapter 11	

Talking It Over with Microsoft Lync

	 3.	 In the instant message window, type your message in the lower text box.
	 4.	 You can change the format, write the message as ink (if you have a drawing tablet

or other pen device), and click Send The Message. (See Figure 11-13.)
	 5.	 When your contact responds, the response appears in the text box at the top of

the message window. You can reply by repeating steps 3 and 4 as often as needed.

Use ink to write your note.
Send an ink message as text.
Type and send the message as text.

Change the format of text.
Add an emoticon.
FIGURE 11-13  Create and format the instant message in the message window.

	

Tip	

If you want to invite others into your conversation, click People Options
in the top right corner of the message window and click Invite By Name
Or Phone Number. Choose the other contacts you want to invite from
the list that appears, and click OK. An invitation is sent to the participants
you selected.

	

Tip	

If you add others to your IM session, you can keep track of who is
i
­nvolved in the conversation by clicking People Options and ­ hoosing
c
Show Participant List. This displays all participants at the top of the
m
­ essage window so that you know who is participating.

	

Instant Messaging with Your Team	

KEEPING A TRANSCRIPT OF YOUR CONVERSATION—OR NOT
By default, Lync keeps a transcript of your conversations so that the content of
your conversations is always available to you for other meetings, projects, or
conversations.
Lync stores your instant message conversations in the Conversation History
folder that is actually available in Outlook Web App. To view past conversations,
display Outlook (by clicking Inbox on the Office 365 Home page) and click the
Conversation History folder in the folder list on the left side of the window. All
your Lync communications that have been tracked appear in this view, and you
can read through conversations to find what you need.
If you want to change the way Lync tracks your conversations—or perhaps turn
off the logging of Lync exchanges—display the Lync Options dialog box by clicking Options in the top right of the Lync window, pointing to Tools, and clicking
Options. Change the Save Instant Message Conversations In My Email Conversation History Folder option by clearing the check box. Similarly, you can stop
keeping track of calls in the same folder by clearing the Save Call Logs In My
Email Conversation History Folder check box.

Chapter 11	

247

	 248	

Chapter 11	

Talking It Over with Microsoft Lync

Making a Call with Lync
When you want to call a contact directly through Lync, you can simply click the contact
in your Lync window. The Call button appears. Click it, and then click in the Subject box
and type the topic of your call. (See Figure 11-14.) When you click Lync Call, Lync dials the
contact’s listed phone number.

FIGURE 11-14  After you click the Call button, type a topic for the call and click Lync Call.

An alert message appears on the other person’s desktop, and the computer “rings” so
that the person can hear the incoming call. When the contact clicks the message box, he
answers the call. During a call, any participant can make the following adjustments while
the conversation is going on, as you see in Figure 11-15:

	 Mute Microphone  Turns off your microphone so that others can’t hear you talk

■

	 Adjust Volume Or Mute Speakers  Changes the volume of the sound you hear

■

	 Display Dial Pad  Shows a number pad so that you can dial a phone number

■

	 Network Connectivity  Shows you how strong your connection is

■

	 Hold  Places the call on hold

■

	 Hang up  Ends the call

■

		
Making a Call with Lync

Hang up

Hold
Display dial pad

Network connectivity

Adjust volume or mute speakers
Mute microphone
FIGURE 11-15  You can change settings while you talk if you want to adjust sound levels or look some-

thing up.

	

Note	

While you are on a call, your Lync status lets others know that you’re busy
and some communication methods are blocked. When you hang up,
your status changes back to available automatically and other ­ ontacts
c
will be able to contact you normally.

Launching a Video Call
If both you and your contact have webcams, you can also have a video call so that you
can each see each other while you talk.
When you set up Lync, you went through the process of preparing your webcam, so the
camera is ready to go whenever you are. When you want to make a video call, follow
these steps:
	 1.	 Click the contact you want to talk to on your video call.
	 2.	 Open the instant messaging window, and send the contact a note if you’d like;

then click Video and click Start Video Call.
	 3.	 A call is made to your contact. He clicks the message box and then clicks Accept

Video Call.
	 4.	 The video appears in the top portion of the message window, and you can

c
­ ontinue sending instant messages in the bottom portion of the window if you
like. (See Figure 11-16.)
	 5.	 When you’re finished with the call, click End Call to hang up.

Chapter 11	

249

	 250	

Chapter 11	

Talking It Over with Microsoft Lync

FIGURE 11-16  You can have a video call and send instant messages at the same time in Lync.

Sharing Programs and Files
Another important task you might need regularly if your team works at a distance
i
­nvolves sharing files, folders, and programs. Suppose that someone on your team wants
to share the design for the latest product brochure. Instead of emailing it to all team
members and then discussing it at some future point, the person presenting the design
idea can open the file on her desktop and then share her desktop with everyone in the
group.
Similarly, if you want to plan a brainstorming session, you can open a whiteboard and
invite everyone you want to attend to put their ideas on the virtual board and see what
stands out.
And if you do want others to have their own copy of the design to go through in more
detail, you can easily transfer files while you’re in Lync so that all of you can literally be on
the same page at the same time.

		
Sharing Programs and Files

Sharing Your Desktop
The process of sharing your desktop is simple. Begin by opening an instant messaging
session with one of your contacts. Click Share in the top of the messaging window, and
click Desktop. Your desktop appears as an extension of the instant messaging window on
your contact’s computer. (See Figure 11-17.)

FIGURE 11-17  Your contact sees the instant messaging conversation on the left and your desktop in the
stage area on the right.

You can transfer control of the screen display by clicking Give Control at the top of your
sharing window and choosing the name of the contact to whom you want to transfer
control. (See Figure 11-18.) You can talk or chat about the contents of the display and
share what you need to share. When you’re finished with the call, click Stop Sharing to
end the sharing of your desktop and return to normal view. You can also click Release
Control to let go of your control of the other user’s computer.

Chapter 11	

251

	

252	

Chapter 11	

Talking It Over with Microsoft Lync

Give another contact control
of the screen display.

Click to stop sharing.

FIGURE 11-18  You can change control of the shared desktop and end sharing when you’re ready by click-

ing the tools at the top of the sharing window.

Sharing Programs
The process of sharing actual programs on your computer is similar to sharing your
desktop. Begin by opening the program you want to share, and then, in the instant
m
­ essaging window, click Share and choose Program. Click the program you want to
share with your contacts, and it appears in the stage area of your screen. You can draw,
write, construct formulas, or do whatever else you want to demonstrate to your group
and, when you’re finished, click Stop Sharing to return your desktop control to only
your view.

		
Sharing Programs and Files

Using a Whiteboard
You can easily create a whiteboard session in which you and your colleagues can
b
­ rainstorm about new ideas, projects, clients, and programs. When you start a
w
­ hiteboard, by clicking Share during an instant messaging session and choosing
New Whiteboard, Lync creates a Group Conversation that enables all participants to have
equal input into what’s happening on the screen. You can add images, add text, doodle,
add shapes, change fonts, and more in the Whiteboard window. (See Figure 11-19.)

FIGURE 11-19  When you share a whiteboard with your contacts, Lync opens a Group Conversation so all

can participate.

You can use the tools at the bottom of the whiteboard to add all kinds of content to the
page, as you see in Figure 11-20.

Chapter 11	

253

	 254	

Chapter 11	

Talking It Over with Microsoft Lync

Laser Pointer
Line Color

Select and Type

Arrow Stamp

Pen

Save with
Annotations

Insert Image

FIGURE 11-20  Use the Whiteboard tools to add content and notes to your developing ideas.

You can save the whiteboard so that you can view it or use the content later by clicking
Save With Annotations at the far right of the Whiteboard tools row. The Save As dialog box appears so that you can navigate to the folder in which you want to store the
w
­ hiteboard. Click Save to save the file.
Of course, you can continue to IM or talk on the phone while you work with the
w
­ hiteboard, so communication continues in several ways at once. Similar to real-time,
around-the-table collaboration, Lync makes it easy to share ideas, work collaboratively,
and finish your projects together in real time.

What’s Next
This chapter introduced you to Microsoft Lync, the great, instant-contact tool that is part
of Office 365. With Lync, you can stay in touch with your team in real time through email,
instant messaging, phone and video calls, desktop and program sharing, and whiteboards. This means you can keep the creative vibe going whether you work down the
hall from your teammates or on different continents.
The next chapter shows you how to design, create, and manage the public-facing ­ ebsite
w
that is yours as part of Office 365.

CHAPTER 12

Designing Your
Public Website
IN THIS CHAPTER

	 Getting started with your public
website

■

	 Choosing a theme and header
style

■

IN ADDITION  to all the tools you’ve already explored in Office 365—
which enable you to organize your contacts, create document ­ibraries,
l
design a team site, stay on top of your email, and communicate in real
time—Office 365 also includes a free public-facing website you can

	 Choosing a custom color scheme

d
­ evelop to present your company to the world. You might use your public

	 Adding and formatting text

website to showcase your products, talk about your services, ­ntroduce
i

	 Inserting, formatting, and
a
­ ligning images

your staff, provide a map to your facility, tell happy customer stories, or

	 Adding and organizing pages

On your public website, you can include text, pictures, video,
a
­ udio, PDFs, and other content. Perhaps your annual report will be
d
­ ownloadable. Maybe visitors can send in their email address to sign
up for your monthly newsletter and get a catalog of your products.

■
■
■

■

	 Selecting a navigation layout

■

	 Adding gadgets to your site

■

	 Optimizing your site for web
search results

■

	 Previewing and publishing your
site

■

	 What’s next

much more.

No matter how you use the site, editing it and enhancing it in
O
­ ffice 365 is a breeze. With a full set of web-based editing tools,
you can control colors, fonts, alignment, and more. This chapter
shows you how.

■

	

Tip	

Do the web tools look familiar? If you previously
used Office Live for Small Business to create,
m
­ anage, and update a website, the web tools
in Office 365 will look familiar to you. Although
some big changes have been made here, the tools
you’ll use are based on the same approach and
structure you learned with Office Live.
255

	 256	

Chapter 12	

Designing Your Public Website

Getting Started with Your Public Website
Your first step in creating your public website involves clicking the Admin tab and
s
­ crolling to the bottom of the Admin Overview page. (Note that you’ll see the Admin
link on the top right of the Office 365 window only if you have Administrator privileges.)
Click the Edit Website link to display your site pages. To begin working on your site, click
Home (your website Home page) and click Edit, as shown in Figure 12-1.

FIGURE 12-1  Click a page, and click Edit to begin making changes to your site.

Making Simple Web Changes
The easiest changes to make on your site involve simple text changes. You can click
in any of the zones you want to change, highlight the text you want to remove, press
Delete, and type the new text. You can use the formatting tools in the Font group of the
Home tab to change the look of the text, and you can use the tools in the Paragraph
group to change the format and alignment of the text on the page. (See Figure 12-2.)

		
Getting Started with Your Public Website

Use to change
the look of text.

Use to change
text alignment.

Highlight text and
change the font or format.

Click and type to
add new content.

Click to resize a zone.

FIGURE 12-2  You can make simple text changes by typing new content and changing the format and

alignment.

	

Tip	

You can view the way your changes will look on your public site by
c
­ licking the View tool in the Site group on the Home tab.

Chapter 12	

257

