mynone = "-"
projects = {"android":"and","moz":"moz","eclipse":"ecl","office":"off"}
top_level = ["press","swe",mynone]
mid_level = {
    "and":"OS",
    "ecl":"IDE",
    "moz":"web",
    "off":"office"
}

def mid_levels(project):
    return [mid_level[project],mynone]

low_level = {
    "and":"dev",
    "ecl":"edoc",
    "off":"odoc",
    "moz":"mdoc"
}

def low_levels(project):
    return [low_level[project], mynone]


def make_name(project, top_level, mid_level, low_level):
    return "%s_%s_%s_%s" % (project, top_level, mid_level, low_level)


def make_csvfile(long_project, name):
    return "data/%s_dataset/%s.csv" % (long_project, name)


def make_csvfile_from(long_project, project, top_level, mid_level, low_level):
    return make_csvfile(long_project, make_csvfile(project, top_level, mid_level, low_level))



def gen_line(long_project, project, top_level, mid_level, low_level):
    data = {
        "long_project":long_project,
        "project":project,
        "top_level":top_level,
        "mid_level":mid_level,
        "low_level":low_level
    }
    low = "data/{d[long_project]}_context_tables/{d[project]}_{d[low_level]}_context_features.csv".format(d=data)
    out = "data/{long_project}_dataset/{project}_{top_level}_{mid_level}_{low_level}.csv".format(**data)
    mid = "data/{long_project}_context_tables/{project}_{mid_level}_context_features.csv".format(**data)
    top = "data/{long_project}_context_tables/{project}_{top_level}.csv".format(**data)
    bmf = "data/{long_project}_context_tables/{project}_bmf_text.csv".format(**data)
    data["low"] = low
    data["out"] = out
    data["mid"] = mid
    data["top"] = top
    data["bmf"] = bmf
    args = list()
    if top_level != mynone:
        args.append('"{top}"')
    if mid_level != mynone:
        args.append('"{mid}"')
    if low_level != mynone:
        args.append('"{low}"')
    # args now is a list of template calls
    print '// %s' % args
    if len(args) == 3:
        """ 3 contexts """
        line = 'createSuperSuperJoinTable("{bmf}", "{top}", "{mid}", "{low}", "{out}")  ;'.format(**data)
        return line
    if len(args) == 2:
        subline = ('createSuperJoinTable("{bmf}", %s, %s, "{out}")  ;' % (args[0],args[1]))
        line = subline.format(**data)
        return line
    if len(args) == 1:
        line = ('createJoinTable("{bmf}", %s, "{out}")  ;' % args[0]).format(**data)
        return line
    if len(args) == 0:
        line = 'createTable("{bmf}", "{out}")  ;'.format(**data)
        return line

for long_project in projects:
    project = projects[long_project]
    for tlevel in top_level:
        for mlevel in mid_levels(project):
            for llevel in low_levels(project):
                print "// %s %s %s %s" % (project, tlevel, mlevel, llevel)
                print gen_line(long_project, project, tlevel, mlevel, llevel)

