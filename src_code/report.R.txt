    project       general      domain        doc        accuracy    
 Android:12   -       :16   -     :24   -      :24   Min.   :88.92  
 Eclipse:12   Pressman:16   IDE   : 6   ProjDoc:24   1st Qu.:91.43  
 Mozilla:12   SWEBOK  :16   Office: 6                Median :92.31  
 Office :12                 OS    : 6                Mean   :92.07  
                            Web   : 6                3rd Qu.:92.79  
                                                     Max.   :93.07  
     kappa       
 Min.   :0.6446  
 1st Qu.:0.7254  
 Median :0.7574  
 Mean   :0.7474  
 3rd Qu.:0.7717  
 Max.   :0.7796  
    project        general       domain         doc         accuracy    
 Android:120   -       :160   -     :240   -      :240   Min.   :88.78  
 Eclipse:120   Pressman:160   IDE   : 60   ProjDoc:240   1st Qu.:91.41  
 Mozilla:120   SWEBOK  :160   Office: 60                 Median :92.34  
 Office :120                  OS    : 60                 Mean   :92.06  
                              Web   : 60                 3rd Qu.:92.77  
                                                         Max.   :93.17  
     kappa       
 Min.   :0.6386  
 1st Qu.:0.7249  
 Median :0.7583  
 Mean   :0.7470  
 3rd Qu.:0.7700  
 Max.   :0.7849  
                       Df  Sum Sq Mean Sq F value   Pr(>F)    
bgeneral                1 0.02596 0.02596   43.47 1.15e-10 ***
bdomain                 1 0.02179 0.02179   36.49 3.11e-09 ***
bdoc                    1 0.03586 0.03586   60.04 5.74e-14 ***
bgeneral:bdomain        1 0.01705 0.01705   28.55 1.43e-07 ***
bgeneral:bdoc           1 0.02240 0.02240   37.51 1.92e-09 ***
bdomain:bdoc            1 0.01099 0.01099   18.39 2.18e-05 ***
bgeneral:bdomain:bdoc   1 0.01351 0.01351   22.62 2.63e-06 ***
Residuals             472 0.28189 0.00060                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
               Df Sum Sq Mean Sq F value   Pr(>F)    
bgeneral        1 0.0260 0.02596   35.80 4.31e-09 ***
bdoc            1 0.0359 0.03586   49.44 7.14e-12 ***
bgeneral:bdoc   1 0.0224 0.02240   30.88 4.57e-08 ***
Residuals     476 0.3452 0.00073                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
             Df Sum Sq  Mean Sq F value   Pr(>F)    
bgeneral      1 0.0260 0.025962   30.76 4.85e-08 ***
Residuals   478 0.4035 0.000844                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
             Df Sum Sq  Mean Sq F value   Pr(>F)    
bdomain       1 0.0218 0.021793   25.55 6.14e-07 ***
Residuals   478 0.4076 0.000853                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
             Df Sum Sq Mean Sq F value  Pr(>F)    
bdoc          1 0.0359 0.03586   43.55 1.1e-10 ***
Residuals   478 0.3936 0.00082                    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
             Df Sum Sq Mean Sq F value  Pr(>F)    
doc           1 0.0359 0.03586   43.55 1.1e-10 ***
Residuals   478 0.3936 0.00082                    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
             Df Sum Sq Mean Sq F value   Pr(>F)    
bdoc          1   35.4   35.44   48.88 9.19e-12 ***
Residuals   478  346.5    0.72                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
                       Df Sum Sq Mean Sq F value   Pr(>F)    
bgeneral                1  26.57   26.57   54.11 8.46e-13 ***
bdomain                 1  21.85   21.85   44.51 7.11e-11 ***
bdoc                    1  35.44   35.44   72.17 2.59e-16 ***
bgeneral:bdomain        1  17.62   17.62   35.88 4.17e-09 ***
bgeneral:bdoc           1  23.15   23.15   47.16 2.07e-11 ***
bdomain:bdoc            1  11.62   11.62   23.67 1.56e-06 ***
bgeneral:bdomain:bdoc   1  13.96   13.96   28.42 1.51e-07 ***
Residuals             472 231.76    0.49                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
               Df Sum Sq Mean Sq F value   Pr(>F)    
bgeneral        1  26.57   26.57   42.61 1.72e-10 ***
bdoc            1  35.44   35.44   56.83 2.42e-13 ***
bgeneral:bdoc   1  23.15   23.15   37.13 2.28e-09 ***
Residuals     476 296.81    0.62                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
                  Df Sum Sq Mean Sq F value   Pr(>F)    
bgeneral           1  26.57  26.569   40.03 5.78e-10 ***
bdomain            1  21.85  21.854   32.93 1.71e-08 ***
bgeneral:bdomain   1  17.62  17.617   26.54 3.78e-07 ***
Residuals        476 315.94   0.664                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
             Df Sum Sq  Mean Sq F value   Pr(>F)    
bdomain       1 0.0218 0.021793   25.55 6.14e-07 ***
Residuals   478 0.4076 0.000853                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ bdomain, data = data)

$bdomain
                 diff         lwr        upr p adj
TRUE-FALSE 0.01347625 0.008237988 0.01871451 6e-07

             Df Sum Sq  Mean Sq F value   Pr(>F)    
bgeneral      1 0.0260 0.025962   30.76 4.85e-08 ***
Residuals   478 0.4035 0.000844                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ bgeneral, data = data)

$bgeneral
                 diff       lwr        upr p adj
TRUE-FALSE 0.01560094 0.0100734 0.02112847     0

             Df Sum Sq Mean Sq F value  Pr(>F)    
bdoc          1 0.0359 0.03586   43.55 1.1e-10 ***
Residuals   478 0.3936 0.00082                    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ bdoc, data = data)

$bdoc
                 diff        lwr        upr p adj
TRUE-FALSE 0.01728625 0.01213915 0.02243335     0

[1] "Is No Context Different than Context?"

	Pairwise comparisons using t tests with pooled SD 

data:  data$kappa and data$bgeneral == TRUE | data$bdomain == TRUE | data$bdoc == TRUE 

     FALSE 
TRUE <2e-16

P value adjustment method: holm 
[1] "Yes!"
[1] "Are all combos of contexts different than no Context?"

	Pairwise comparisons using t tests with pooled SD 

data:  data$kappa and paste(data$bgeneral, data$bdomain, data$bdoc) 

                 FALSE FALSE FALSE FALSE FALSE TRUE FALSE TRUE FALSE
FALSE FALSE TRUE <2e-16            -                -               
FALSE TRUE FALSE <2e-16            1.000            -               
FALSE TRUE TRUE  <2e-16            1.000            0.478           
TRUE FALSE FALSE <2e-16            1.000            1.000           
TRUE FALSE TRUE  <2e-16            1.000            1.000           
TRUE TRUE FALSE  <2e-16            1.000            1.000           
TRUE TRUE TRUE   <2e-16            1.000            0.240           
                 FALSE TRUE TRUE TRUE FALSE FALSE TRUE FALSE TRUE
FALSE FALSE TRUE -               -                -              
FALSE TRUE FALSE -               -                -              
FALSE TRUE TRUE  -               -                -              
TRUE FALSE FALSE 0.129           -                -              
TRUE FALSE TRUE  1.000           0.237            -              
TRUE TRUE FALSE  1.000           1.000            1.000          
TRUE TRUE TRUE   1.000           0.023            1.000          
                 TRUE TRUE FALSE
FALSE FALSE TRUE -              
FALSE TRUE FALSE -              
FALSE TRUE TRUE  -              
TRUE FALSE FALSE -              
TRUE FALSE TRUE  -              
TRUE TRUE FALSE  -              
TRUE TRUE TRUE   1.000          

P value adjustment method: holm 
[1] "Yes! Except for just domain p-value at 0.1139"
[1] "Is kappa affected by ALL Context"
                       Df  Sum Sq Mean Sq F value   Pr(>F)    
bdoc                    1 0.03586 0.03586   60.04 5.74e-14 ***
bdomain                 1 0.02179 0.02179   36.49 3.11e-09 ***
bgeneral                1 0.02596 0.02596   43.47 1.15e-10 ***
bdoc:bdomain            1 0.01099 0.01099   18.39 2.18e-05 ***
bdoc:bgeneral           1 0.02240 0.02240   37.51 1.92e-09 ***
bdomain:bgeneral        1 0.01705 0.01705   28.55 1.43e-07 ***
bdoc:bdomain:bgeneral   1 0.01351 0.01351   22.62 2.63e-06 ***
Residuals             472 0.28189 0.00060                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ bdoc * bdomain * bgeneral, data = data)

$bdoc
                 diff        lwr        upr p adj
TRUE-FALSE 0.01728625 0.01290256 0.02166994     0

$bdomain
                 diff         lwr        upr p adj
TRUE-FALSE 0.01347625 0.009092556 0.01785994     0

$bgeneral
                 diff        lwr        upr p adj
TRUE-FALSE 0.01560094 0.01095133 0.02025055     0

$`bdoc:bdomain`
                               diff           lwr         upr     p adj
TRUE:FALSE-FALSE:FALSE  0.026854167  0.0187200625 0.034988271 0.0000000
FALSE:TRUE-FALSE:FALSE  0.023044167  0.0149100625 0.031178271 0.0000000
TRUE:TRUE-FALSE:FALSE   0.030762500  0.0226283959 0.038896604 0.0000000
FALSE:TRUE-TRUE:FALSE  -0.003810000 -0.0119441041 0.004324104 0.6222969
TRUE:TRUE-TRUE:FALSE    0.003908333 -0.0042257708 0.012042437 0.6024534
TRUE:TRUE-FALSE:TRUE    0.007718333 -0.0004157708 0.015852437 0.0699975

$`bdoc:bgeneral`
                               diff           lwr         upr     p adj
TRUE:FALSE-FALSE:FALSE  0.036607500  0.0266452977 0.046569702 0.0000000
FALSE:TRUE-FALSE:FALSE  0.030091875  0.0214643547 0.038719395 0.0000000
TRUE:TRUE-FALSE:FALSE   0.037717500  0.0290899797 0.046345020 0.0000000
FALSE:TRUE-TRUE:FALSE  -0.006515625 -0.0151431453 0.002111895 0.2099193
TRUE:TRUE-TRUE:FALSE    0.001110000 -0.0075175203 0.009737520 0.9874018
TRUE:TRUE-FALSE:TRUE    0.007625625  0.0005812842 0.014669966 0.0279188

$`bdomain:bgeneral`
                               diff          lwr         upr     p adj
TRUE:FALSE-FALSE:FALSE  0.030332500  0.020370298 0.040294702 0.0000000
FALSE:TRUE-FALSE:FALSE  0.028243125  0.019615605 0.036870645 0.0000000
TRUE:TRUE-FALSE:FALSE   0.033291250  0.024663730 0.041918770 0.0000000
FALSE:TRUE-TRUE:FALSE  -0.002089375 -0.010716895 0.006538145 0.9242336
TRUE:TRUE-TRUE:FALSE    0.002958750 -0.005668770 0.011586270 0.8130903
TRUE:TRUE-FALSE:TRUE    0.005048125 -0.001996216 0.012092466 0.2524442

$`bdoc:bdomain:bgeneral`
                                          diff           lwr         upr
TRUE:FALSE:FALSE-FALSE:FALSE:FALSE  0.06118000  0.0445428200 0.077817180
FALSE:TRUE:FALSE-FALSE:FALSE:FALSE  0.05490500  0.0382678200 0.071542180
TRUE:TRUE:FALSE-FALSE:FALSE:FALSE   0.06694000  0.0503028200 0.083577180
FALSE:FALSE:TRUE-FALSE:FALSE:FALSE  0.05398750  0.0395792794 0.068395721
TRUE:FALSE:TRUE-FALSE:FALSE:FALSE   0.06367875  0.0492705294 0.078086971
FALSE:TRUE:TRUE-FALSE:FALSE:FALSE   0.06110125  0.0466930294 0.075509471
TRUE:TRUE:TRUE-FALSE:FALSE:FALSE    0.06666125  0.0522530294 0.081069471
FALSE:TRUE:FALSE-TRUE:FALSE:FALSE  -0.00627500 -0.0229121800 0.010362180
TRUE:TRUE:FALSE-TRUE:FALSE:FALSE    0.00576000 -0.0108771800 0.022397180
FALSE:FALSE:TRUE-TRUE:FALSE:FALSE  -0.00719250 -0.0216007206 0.007215721
TRUE:FALSE:TRUE-TRUE:FALSE:FALSE    0.00249875 -0.0119094706 0.016906971
FALSE:TRUE:TRUE-TRUE:FALSE:FALSE   -0.00007875 -0.0144869706 0.014329471
TRUE:TRUE:TRUE-TRUE:FALSE:FALSE     0.00548125 -0.0089269706 0.019889471
TRUE:TRUE:FALSE-FALSE:TRUE:FALSE    0.01203500 -0.0046021800 0.028672180
FALSE:FALSE:TRUE-FALSE:TRUE:FALSE  -0.00091750 -0.0153257206 0.013490721
TRUE:FALSE:TRUE-FALSE:TRUE:FALSE    0.00877375 -0.0056344706 0.023181971
FALSE:TRUE:TRUE-FALSE:TRUE:FALSE    0.00619625 -0.0082119706 0.020604471
TRUE:TRUE:TRUE-FALSE:TRUE:FALSE     0.01175625 -0.0026519706 0.026164471
FALSE:FALSE:TRUE-TRUE:TRUE:FALSE   -0.01295250 -0.0273607206 0.001455721
TRUE:FALSE:TRUE-TRUE:TRUE:FALSE    -0.00326125 -0.0176694706 0.011146971
FALSE:TRUE:TRUE-TRUE:TRUE:FALSE    -0.00583875 -0.0202469706 0.008569471
TRUE:TRUE:TRUE-TRUE:TRUE:FALSE     -0.00027875 -0.0146869706 0.014129471
TRUE:FALSE:TRUE-FALSE:FALSE:TRUE    0.00969125 -0.0020730128 0.021455513
FALSE:TRUE:TRUE-FALSE:FALSE:TRUE    0.00711375 -0.0046505128 0.018878013
TRUE:TRUE:TRUE-FALSE:FALSE:TRUE     0.01267375  0.0009094872 0.024438013
FALSE:TRUE:TRUE-TRUE:FALSE:TRUE    -0.00257750 -0.0143417628 0.009186763
TRUE:TRUE:TRUE-TRUE:FALSE:TRUE      0.00298250 -0.0087817628 0.014746763
TRUE:TRUE:TRUE-FALSE:TRUE:TRUE      0.00556000 -0.0062042628 0.017324263
                                       p adj
TRUE:FALSE:FALSE-FALSE:FALSE:FALSE 0.0000000
FALSE:TRUE:FALSE-FALSE:FALSE:FALSE 0.0000000
TRUE:TRUE:FALSE-FALSE:FALSE:FALSE  0.0000000
FALSE:FALSE:TRUE-FALSE:FALSE:FALSE 0.0000000
TRUE:FALSE:TRUE-FALSE:FALSE:FALSE  0.0000000
FALSE:TRUE:TRUE-FALSE:FALSE:FALSE  0.0000000
TRUE:TRUE:TRUE-FALSE:FALSE:FALSE   0.0000000
FALSE:TRUE:FALSE-TRUE:FALSE:FALSE  0.9456093
TRUE:TRUE:FALSE-TRUE:FALSE:FALSE   0.9656192
FALSE:FALSE:TRUE-TRUE:FALSE:FALSE  0.7965661
TRUE:FALSE:TRUE-TRUE:FALSE:FALSE   0.9995110
FALSE:TRUE:TRUE-TRUE:FALSE:FALSE   1.0000000
TRUE:TRUE:TRUE-TRUE:FALSE:FALSE    0.9431160
TRUE:TRUE:FALSE-FALSE:TRUE:FALSE   0.3521229
FALSE:FALSE:TRUE-FALSE:TRUE:FALSE  0.9999995
TRUE:FALSE:TRUE-FALSE:TRUE:FALSE   0.5833689
FALSE:TRUE:TRUE-FALSE:TRUE:FALSE   0.8951791
TRUE:TRUE:TRUE-FALSE:TRUE:FALSE    0.2047181
FALSE:FALSE:TRUE-TRUE:TRUE:FALSE   0.1142182
TRUE:FALSE:TRUE-TRUE:TRUE:FALSE    0.9972612
FALSE:TRUE:TRUE-TRUE:TRUE:FALSE    0.9215308
TRUE:TRUE:TRUE-TRUE:TRUE:FALSE     1.0000000
TRUE:FALSE:TRUE-FALSE:FALSE:TRUE   0.1944793
FALSE:TRUE:TRUE-FALSE:FALSE:TRUE   0.5922809
TRUE:TRUE:TRUE-FALSE:FALSE:TRUE    0.0245176
FALSE:TRUE:TRUE-TRUE:FALSE:TRUE    0.9977721
TRUE:TRUE:TRUE-TRUE:FALSE:TRUE     0.9944429
TRUE:TRUE:TRUE-FALSE:TRUE:TRUE     0.8386945

[1] "Is kappa affected by Project Specific Context"
             Df Sum Sq Mean Sq F value  Pr(>F)    
bdoc          1 0.0359 0.03586   43.55 1.1e-10 ***
Residuals   478 0.3936 0.00082                    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ bdoc, data = data)

$bdoc
                 diff        lwr        upr p adj
TRUE-FALSE 0.01728625 0.01213915 0.02243335     0

[1] "Is kappa affected by Domain Specific Context"
             Df Sum Sq  Mean Sq F value   Pr(>F)    
bdomain       1 0.0218 0.021793   25.55 6.14e-07 ***
Residuals   478 0.4076 0.000853                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ bdomain, data = data)

$bdomain
                 diff         lwr        upr p adj
TRUE-FALSE 0.01347625 0.008237988 0.01871451 6e-07

[1] "Is kappa affected by General Context"
             Df Sum Sq  Mean Sq F value   Pr(>F)    
bgeneral      1 0.0260 0.025962   30.76 4.85e-08 ***
Residuals   478 0.4035 0.000844                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ bgeneral, data = data)

$bgeneral
                 diff       lwr        upr p adj
TRUE-FALSE 0.01560094 0.0100734 0.02112847     0

[1] "Is kappa affected by which General Context"
             Df Sum Sq  Mean Sq F value   Pr(>F)    
general       2 0.0264 0.013181    15.6 2.74e-07 ***
Residuals   477 0.4031 0.000845                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = kappa ~ general, data = data)

$general
                        diff          lwr         upr     p adj
Pressman--       0.016720625  0.009079581 0.024361669 0.0000012
SWEBOK--         0.014481250  0.006840206 0.022122294 0.0000310
SWEBOK-Pressman -0.002239375 -0.009880419 0.005401669 0.7700169

[1] "Accuracy: Is No Context Different than Context?"

	Pairwise comparisons using t tests with pooled SD 

data:  data$accuracy and data$bgeneral == TRUE | data$bdomain == TRUE | data$bdoc == TRUE 

     FALSE 
TRUE <2e-16

P value adjustment method: holm 
[1] "Yes!"
[1] "Accuracy: Are all combos of contexts different than no Context?"

	Pairwise comparisons using t tests with pooled SD 

data:  data$accuracy and paste(data$bgeneral, data$bdomain, data$bdoc) 

                 FALSE FALSE FALSE FALSE FALSE TRUE FALSE TRUE FALSE
FALSE FALSE TRUE <2e-16            -                -               
FALSE TRUE FALSE <2e-16            1.000            -               
FALSE TRUE TRUE  <2e-16            1.000            0.310           
TRUE FALSE FALSE <2e-16            1.000            1.000           
TRUE FALSE TRUE  <2e-16            1.000            0.673           
TRUE TRUE FALSE  <2e-16            1.000            1.000           
TRUE TRUE TRUE   <2e-16            1.000            0.150           
                 FALSE TRUE TRUE TRUE FALSE FALSE TRUE FALSE TRUE
FALSE FALSE TRUE -               -                -              
FALSE TRUE FALSE -               -                -              
FALSE TRUE TRUE  -               -                -              
TRUE FALSE FALSE 0.067           -                -              
TRUE FALSE TRUE  1.000           0.123            -              
TRUE TRUE FALSE  1.000           0.673            1.000          
TRUE TRUE TRUE   1.000           0.010            1.000          
                 TRUE TRUE FALSE
FALSE FALSE TRUE -              
FALSE TRUE FALSE -              
FALSE TRUE TRUE  -              
TRUE FALSE FALSE -              
TRUE FALSE TRUE  -              
TRUE TRUE FALSE  -              
TRUE TRUE TRUE   1.000          

P value adjustment method: holm 
[1] "Yes! Except for just domain p-value at 0.1139"
[1] "Is accuracy affected by Project Specific Context"
             Df Sum Sq Mean Sq F value   Pr(>F)    
bdoc          1   35.4   35.44   48.88 9.19e-12 ***
Residuals   478  346.5    0.72                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = accuracy ~ bdoc, data = data)

$bdoc
                diff     lwr       upr p adj
TRUE-FALSE 0.5434383 0.39071 0.6961667     0

[1] "Is accuracy affected by Domain Specific Context"
             Df Sum Sq Mean Sq F value   Pr(>F)    
bdomain       1   21.9  21.854   29.01 1.13e-07 ***
Residuals   478  360.1   0.753                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = accuracy ~ bdomain, data = data)

$bdomain
                diff      lwr       upr p adj
TRUE-FALSE 0.4267542 0.271061 0.5824473 1e-07

[1] "Is accuracy affected by General Context"
             Df Sum Sq Mean Sq F value   Pr(>F)    
bgeneral      1   26.6  26.569   35.73 4.43e-09 ***
Residuals   478  355.4   0.744                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = accuracy ~ bgeneral, data = data)

$bgeneral
                diff       lwr       upr p adj
TRUE-FALSE 0.4990863 0.3350334 0.6631391     0

[1] "Is accuracy affected by which General Context"
             Df Sum Sq Mean Sq F value   Pr(>F)    
general       2   26.9  13.436   18.05 2.78e-08 ***
Residuals   477  355.1   0.744                     
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
  Tukey multiple comparisons of means
    95% family-wise confidence level

Fit: aov(formula = accuracy ~ general, data = data)

$general
                      diff        lwr       upr     p adj
Pressman--       0.5298787  0.3030827 0.7566748 0.0000002
SWEBOK--         0.4682938  0.2414977 0.6950898 0.0000049
SWEBOK-Pressman -0.0615850 -0.2883811 0.1652111 0.7989901

[1] "Android DEV"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.7530  0.8820  0.8311  0.9810  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.6640  0.7960  0.7426  0.8850  1.0000 
[1] 0.086
[1] "Android OS"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9440  0.9720  0.9557  0.9910  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9330  0.9565  0.9430  0.9720  1.0000 
[1] 0.0155
[1] "Android SWEBOK"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.1200  0.9260  0.9590  0.9441  0.9900  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9050  0.9400  0.9203  0.9620  1.0000 
[1] 0.019
[1] "Android SWEBOK DEV"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.1170  0.9170  0.9530  0.9377  0.9880  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8880  0.9270  0.9073  0.9540  1.0000 
[1] 0.026
[1] "Android SWEBOK OS"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.1480  0.9268  0.9570  0.9457  0.9880  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.911   0.940   0.925   0.959   1.000 
[1] 0.017
[1] "Android SWEBOK OS DEV"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.1480  0.9230  0.9550  0.9431  0.9870  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9040  0.9350  0.9195  0.9550  1.0000 
[1] 0.02
[1] "Android Pressman"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9050  0.9540  0.9285  0.9890  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8920  0.9350  0.9077  0.9610  1.0000 
[1] 0.019
[1] "Android Pressman DEV"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8830  0.9390  0.9143  0.9840  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8510  0.9070  0.8775  0.9420  1.0000 
[1] 0.032
[1] "Android Pressman OS"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9238  0.9580  0.9436  0.9870  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9120  0.9420  0.9265  0.9610  1.0000 
[1] 0.016
[1] "Android Pressman OS DEV"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.919   0.954   0.940   0.986   1.000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9020  0.9340  0.9191  0.9560  1.0000 
[1] 0.02
[1] "Office ODOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9380  0.9610  0.9466  0.9750  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9280  0.9540  0.9377  0.9700  1.0000 
[1] 0.007
[1] "Office office"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9000  0.9480  0.9096  0.9720  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.885   0.939   0.897   0.966   1.000 
[1] 0.009
[1] "Office SWEBOK"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8690  0.9230  0.8923  0.9530  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8570  0.9130  0.8825  0.9470  1.0000 
[1] 0.01
[1] "Office SWEBOK office"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8510  0.9110  0.8815  0.9460  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8330  0.8980  0.8671  0.9360  1.0000 
[1] 0.013
[1] "Office SWEBOK odoc"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9150  0.9460  0.9298  0.9640  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.905   0.937   0.919   0.958   1.000 
[1] 0.009
[1] "Office SWEBOK office ODOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9035  0.9380  0.9204  0.9600  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8900  0.9270  0.9084  0.9510  1.0000 
[1] 0.011
[1] "Office Pressman"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8470  0.9130  0.8714  0.9500  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8330  0.9040  0.8572  0.9430  1.0000 
[1] 0.009
[1] "Office Pressman office"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8520  0.9150  0.8775  0.9500  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8300  0.9010  0.8626  0.9400  1.0000 
[1] 0.014
[1] "Office Pressman odoc"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9240  0.9510  0.9362  0.9680  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9130  0.9430  0.9259  0.9620  1.0000 
[1] 0.008
[1] "Office Pressman office ODOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9090  0.9420  0.9248  0.9630  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8960  0.9320  0.9131  0.9550  1.0000 
[1] 0.01
[1] "Mozilla MDOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8610  0.9160  0.8903  0.9510  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8640  0.9190  0.8932  0.9530  1.0000 
[1] -0.003
[1] "Mozilla web"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.942   0.964   0.951   0.978   1.000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9430  0.9650  0.9508  0.9780  1.0000 
[1] -0.001
[1] "Mozilla SWEBOK"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8700  0.9210  0.8934  0.9520  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8710  0.9210  0.8937  0.9510  1.0000 
[1] 0
[1] "Mozilla SWEBOK web"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9150  0.9460  0.9304  0.9650  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9160  0.9470  0.9308  0.9650  1.0000 
[1] -0.001
[1] "Mozilla SWEBOK mdoc"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.844   0.898   0.875   0.935   1.000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8450  0.8990  0.8769  0.9360  1.0000 
[1] -0.001
[1] "Mozilla SWEBOK mozilla MDOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8990  0.9340  0.9179  0.9570  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9010  0.9350  0.9186  0.9570  1.0000 
[1] -0.001
[1] "Mozilla Pressman"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8530  0.9140  0.8751  0.9490  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8540  0.9150  0.8759  0.9490  1.0000 
[1] -0.001
[1] "Mozilla Pressman web"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9270  0.9540  0.9391  0.9710  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9280  0.9540  0.9392  0.9700  1.0000 
[1] 0
[1] "Mozilla Pressman mdoc"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8360  0.8950  0.8703  0.9340  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8390  0.8980  0.8729  0.9360  1.0000 
[1] -0.003
[1] "Mozilla Pressman mozilla MDOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9070  0.9390  0.9235  0.9610  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.908   0.940   0.924   0.961   1.000 
[1] -0.001
[1] "Eclipse EDOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9320  0.9610  0.9449  0.9770  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9140  0.9470  0.9284  0.9680  1.0000 
[1] 0.014
[1] "Eclipse eclipse"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9150  0.9490  0.9313  0.9720  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8990  0.9390  0.9166  0.9640  1.0000 
[1] 0.01
[1] "Eclipse SWEBOK"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9170  0.9500  0.9332  0.9720  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
  0.000   0.898   0.938   0.917   0.962   1.000 
[1] 0.012
[1] "Eclipse SWEBOK edoc"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9010  0.9410  0.9238  0.9670  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8760  0.9220  0.9027  0.9510  1.0000 
[1] 0.019
[1] "Eclipse SWEBOK IDE"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9020  0.9410  0.9233  0.9650  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8800  0.9250  0.9043  0.9520  1.0000 
[1] 0.016
[1] "Eclipse SWEBOK eclipse EDOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8940  0.9360  0.9189  0.9620  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8690  0.9150  0.8973  0.9460  1.0000 
[1] 0.021
[1] "Eclipse Pressman"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9070  0.9460  0.9211  0.9690  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8840  0.9330  0.9028  0.9600  1.0000 
[1] 0.013
[1] "Eclipse Pressman edoc"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.9060  0.9450  0.9266  0.9680  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8820  0.9260  0.9059  0.9540  1.0000 
[1] 0.019
[1] "Eclipse Pressman IDE"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8970  0.9360  0.9171  0.9620  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8740  0.9210  0.8981  0.9510  1.0000 
[1] 0.015
[1] "Eclipse Pressman eclipse EDOC"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8940  0.9360  0.9182  0.9620  1.0000 
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.0000  0.8680  0.9160  0.8968  0.9470  1.0000 
[1] 0.02
 [1]  0.0860  0.0155  0.0190  0.0260  0.0170  0.0200  0.0190  0.0320  0.0160
[10]  0.0200  0.0070  0.0090  0.0100  0.0130  0.0090  0.0110  0.0090  0.0140
[19]  0.0080  0.0100 -0.0030 -0.0010  0.0000 -0.0010 -0.0010 -0.0010 -0.0010
[28]  0.0000 -0.0030 -0.0010  0.0140  0.0100  0.0120  0.0190  0.0160  0.0210
[37]  0.0130  0.0190  0.0150  0.0200
    Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
-0.00300  0.00525  0.01250  0.01294  0.01900  0.08600 
